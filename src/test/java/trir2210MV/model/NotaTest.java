package trir2210MV.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import trir2210MV.controller.NoteController;
import trir2210MV.main.ClasaException;
import trir2210MV.repository.ClasaRepositoryMock;

import java.util.ArrayList;
import java.util.List;


import static org.junit.Assert.*;

public class NotaTest {

    private Nota n1;
    private Nota n2;
    private Nota n3;
    private Nota n4;
    private Nota n5;
    private Nota n6;
    private Nota n7;
    private NoteController ctrl;
    private ClasaRepositoryMock repo;

    @Before
    public void setUp() throws Exception {

        n1 = new Nota(1, "engleza", 10);
        n2 = new Nota(1, "informatica", 8);
        n3 = new Nota(1, "matematica", 11);
        n4 = new Nota(2,"abcd", 6);
        n5 = new Nota(2,"matematica", 0);
        n6 = new Nota(1, "cevamaimaredecatdouazeci", 8);
        n7 = new Nota(2, "aaaaaaaaaaaaaaaaa", 7);
        ctrl = new NoteController();
        repo = new ClasaRepositoryMock();

    }

    @After
    public void tearDown() throws Exception {
    }

    @Rule
    public ExpectedException ex = ExpectedException.none();



    @Test
    public void testAddNotaECP_v() throws Exception{
        try{

            int prevSize = ctrl.getNote().size();
            ctrl.addNota(n1);
            ctrl.addNota(n7);
            int size = ctrl.getNote().size();
            assertEquals(prevSize+2, size);

        }catch (ClasaException ex){ex.printStackTrace();}
    }

    @Test(expected = ClasaException.class)
    public void testAddNotaECP_n() throws ClasaException{
        ctrl.addNota(n3);
        ctrl.addNota(n4);
        ctrl.addNota(n5);
    }

    @Test
    public void testAddNotaBVA_v() throws Exception{
        int prevS = ctrl.getNote().size();
        ctrl.addNota(n1);
        ctrl.addNota(n2);
        int size = ctrl.getNote().size();
        assertEquals(prevS +2, size);
    }

    @Test(expected = ClasaException.class)
    public void testAddNotaBVA_n() throws ClasaException {
        // ctrl.addNota(n3);
        ctrl.addNota(n4);
        ctrl.addNota(n6);
    }

    @Test
    public void testCalculMedieWB_v() throws Exception {
        try{

            List<Elev> e = new ArrayList<>();
            e.add(new Elev(1, "Ramona"));
            e.add(new Elev(2,"Ioana"));
            List<Nota> n = new ArrayList<>();
            n.add(new Nota (1,"informatica", 10));
            n.add(new Nota (1,"informatica", 7));
            n.add(new Nota (2, "matematica",8));
            n.add(new Nota (2, "matematica",5));
            n.add(new Nota (1,"informatica", 10));
            n.add(new Nota (1,"informatica", 7));
            n.add(new Nota (2, "matematica",8));
            n.add(new Nota (2, "matematica",5));
            repo.creazaClasa(e,n);
            List<Medie> m = repo.calculeazaMedii();
            double m1 = 8.5;
            double m2 = 6.5;
            if (m.get(0).getElev().getNrmatricol()==1){
                assertEquals(m1, m.get(0).getMedie(), 0.0);
                assertEquals(m2, m.get(1).getMedie(), 0.0);
            }
            else{
                assertEquals(m2, m.get(0).getMedie(), 0.0);
                assertEquals(m1, m.get(1).getMedie(), 0.0);
            }

        }catch (ClasaException ex){
            ex.printStackTrace();
        }
    }


    @Test
    public void testCalculMedieWB_n() throws ClasaException{
        ex.expect(ClasaException.class);
        ClasaRepositoryMock clasaRepositoryMock = new ClasaRepositoryMock();
        clasaRepositoryMock.calculeazaMedii();
    }

    @Test
    public void testGetCorigenti_v(){
        ClasaRepositoryMock clasaRepositoryMock=new ClasaRepositoryMock();
        List<Elev> elevi = new ArrayList<>();
        elevi.add(new Elev(1, "Ramona"));
        elevi.add(new Elev(2,"Ioana"));
        List<Nota> note = new ArrayList<>();
        note.add(new Nota(1, "informatica", 1));
        note.add(new Nota(1, "informatica", 1));
        note.add(new Nota(2,"informatica",3));
        note.add(new Nota(2,"matematica",4));
        clasaRepositoryMock.creazaClasa(elevi,note);
        List<Corigent> corigenti=clasaRepositoryMock.getCorigenti();
        assertEquals(corigenti.get(0).getNumeElev(),"Ramona");
        assertEquals(corigenti.get(1).getNumeElev(),"Ioana");
    }

   /* @Test
    public void testGetCorigenti_n() throws ClasaException{
        ex.expect(ClasaException.class);
        ClasaRepositoryMock clasaRepositoryMock=new ClasaRepositoryMock();
        clasaRepositoryMock.getCorigenti();
    }*/




}