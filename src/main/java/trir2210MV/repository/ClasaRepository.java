package trir2210MV.repository;

import trir2210MV.main.ClasaException;
import trir2210MV.model.Corigent;
import trir2210MV.model.Elev;
import trir2210MV.model.Medie;
import trir2210MV.model.Nota;

import java.util.HashMap;
import java.util.List;

public interface ClasaRepository {
    public void creazaClasa(List<Elev> elevi, List<Nota> note);
    public HashMap<Elev, HashMap<String, List<Double>>> getClasa();
    public List<Medie> calculeazaMedii() throws ClasaException;
    public void afiseazaClasa();
    public List<Corigent> getCorigenti();
}
