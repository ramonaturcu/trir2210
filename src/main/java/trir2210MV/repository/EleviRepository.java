package trir2210MV.repository;

import trir2210MV.model.Elev;

import java.util.List;

public interface EleviRepository {
    public void addElev(Elev e);
    public List<Elev> getElevi();
    public void readElevi(String fisier);
}
