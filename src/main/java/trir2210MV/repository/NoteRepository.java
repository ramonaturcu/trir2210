package trir2210MV.repository;

import trir2210MV.main.ClasaException;
import trir2210MV.model.Nota;

import java.util.List;

public interface NoteRepository {
    public void addNota(Nota nota) throws ClasaException;
    public List<Nota> getNote();
    public void readNote(String fisier);
}
