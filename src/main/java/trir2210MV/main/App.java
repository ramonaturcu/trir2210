package trir2210MV.main;

import trir2210MV.controller.NoteController;
import trir2210MV.model.Corigent;
import trir2210MV.model.Medie;
import trir2210MV.model.Nota;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    /**
     * @param args
     * @throws ClasaException
     */
    public static void main(String[] args) throws ClasaException {
        // TODO Auto-generated method stub
        NoteController ctrl = new NoteController();
        List<Medie> medii = new LinkedList<Medie>();
        List<Corigent> corigenti = new ArrayList<Corigent>();
        ctrl.readElevi(args[0]);
        ctrl.readNote(args[1]);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        boolean gasit = false;
        while(!gasit) {
            System.out.println("1. Adaugare Nota");
            System.out.println("2. Calculeaza medii");
            System.out.println("3. Elevi corigenti");
            System.out.println("4. Iesire");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in) );
            try {
                int option = Integer.parseInt(br.readLine());
                switch(option) {
                    case 1:
                        System.out.println("Introduceti numarul matricol:");
                        BufferedReader readN = new BufferedReader(new InputStreamReader(System.in));
                        System.out.println("Introduceti materia: ");
                        BufferedReader readMat = new BufferedReader(new InputStreamReader(System.in));
                        System.out.println("Introduceti nota: ");
                        BufferedReader readNota = new BufferedReader(new InputStreamReader(System.in));
                        String inputN = readN.readLine();
                        Double nr = Double.parseDouble(inputN);
                        String inputMat = readMat.readLine();
                        String inputNota = readNota.readLine();
                        Double nota = Double.parseDouble(inputNota);
                        Nota notaAdd = new Nota(nr,inputMat,nota);
                        ctrl.addNota(notaAdd);
                        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
                        break;

                    case 2: medii = ctrl.calculeazaMedii();
                        for(Medie medie:medii)
                            System.out.println(medie);
                        break;
                    case 3: corigenti = ctrl.getCorigenti();
                        for(Corigent corigent:corigenti)
                            System.out.println(corigent);
                        break;
                    case 4: gasit = true;
                        break;
                    default: System.out.println("Introduceti o optiune valida!");
                }

            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (ClasaException e){e.printStackTrace();}
        }
    }
}
